package main

import (
	"fmt"
	"log"
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to read 32-bit number with writing of message */
func readInt32(msg string, number *int32) {

	/* declaration of variables */
	var (
		err error
	)

	/* print message and read number */
	fmt.Println(msg)
	_, err = fmt.Scanln(number)
	handleError(err)

}

/* print bits of 32-bit number */
func printAsBits(number int32) {

	/* declaration of variables */
	var (
		div int32
		err error
	)

	/* write the first bit */
	div = -2147483648
	_, err = fmt.Print((number & div) / div)

	/* write next bits */
	div = 1073741824
	for div >= 1 {
		_, err = fmt.Print((number & div) / div)
		handleError(err)
		div /= 2
	}

}

/* function to print all registers */
func printAllRegisters(m int32, a int32, q int32) {

	/* print all registers */
	fmt.Print("register \"m\": ")
	printAsBits(m)
	fmt.Println()
	fmt.Print("register \"a_q\": ")
	printAsBits(a)
	printAsBits(q)
	fmt.Println()
	fmt.Println()

}

/* entry point */
func main() {

	/* declaration of variables */
	var (
		m             int32
		a,q           int32
		counter       int32
		upperBitQ     int32
		iter          int32
		signA         int32
		signM         int32
		signQ         int32
	)

	/* read number for registers */
	readInt32("read \"m\" number:", &m)
	readInt32("read \"q\" number:", &q)
	fmt.Println()

	/* receive signs of numbers */
	if m >= 0 {
		signM = 1
	} else {
		m *= -1
		signM = -1
	}
	if q >= 0 {
		signQ = 1
	} else {
		q *= -1
		signQ = -1
	}

	/* initial values */
	a = 0
	iter = 0
	counter = 32
	fmt.Println("a=0, q = q_init, m = m_init, counter=32")

	/* print registers and iteration */
	fmt.Print("iteration: " )
	fmt.Println(iter)
	printAllRegisters(m, a, q)

	/* make bitwise */
	upperBitQ = (q & (-2147483648)) / int32(-2147483648)
	a = (a & 2147483647) << 1 + upperBitQ
	q = (q & 2147483647) << 1
	fmt.Println("make bitwise \"a_q\" register")

	/* save sing of register "a" */
	if a >= 0 {
		signA = 1
	} else {
		signA = -1
	}
	fmt.Println("save sign of \"a\" register")

	/* subtract value and check correctness of this operation */
	a -= m
	fmt.Println("subtract value \"m\" using register \"a\"")
	if (signA * a >= 0) || ((a == 0) && (q == 0)) {
		q += 1
		fmt.Println("\"a\" register does not change sign, so \"q_0\" is 1")
	} else {
		a += m
		fmt.Println("\"a\" register does not change sign, so \"q_0\" is 0 and a returns to previous state")
	}

	/* calculate iteration and counter */
	iter++
	counter--
	fmt.Println("iteration was ended")

	/* print registers and iteration */
	fmt.Print("iteration: " )
	fmt.Println(iter)
	printAllRegisters(m, a, q)

	/* launch loop to divide */
	for counter != 0 {

		/* make bitwise */
		upperBitQ = (q & (-2147483648)) / int32(-2147483648)
		a = (a & 2147483647) << 1 + upperBitQ
		q = (q & 2147483647) << 1
		fmt.Println("make bitwise \"a_q\" register")

		/* save sing of register "a" */
		if a >= 0 {
			signA = 1
		} else {
			signA = -1
		}
		fmt.Println("save sign of \"a\" register")

		/* subtract value and check correctness of this operation */
		a -= m
		fmt.Println("subtract value \"m\" using register \"a\"")
		if (signA * a >= 0) || ((a == 0) && (q == 0)) {
			q += 1
			fmt.Println("\"a\" register does not change sign, so \"q_0\" is 1")
		} else {
			a += m
			fmt.Println("\"a\" register does not change sign, so \"q_0\" is 0 and a returns to previous state")
		}

		/* calculate iteration and counter */
		iter++
		counter--
		fmt.Println("iteration was ended")

		/* print registers and iteration */
		fmt.Print("iteration: " )
		fmt.Println(iter)
		printAllRegisters(m, a, q)

	}

	/* write final result */
	fmt.Println("final result")
	fmt.Println("register a:")
	fmt.Println(a)
	fmt.Println("register q:")
	fmt.Println(signQ * signM * q)

}
